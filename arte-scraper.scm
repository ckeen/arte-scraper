(use (srfi 1 14 19)
     html-parser
     http-client
     intarweb
     irregex
     numbers
     parley
     parley-auto-completion
     posix
     progress-indicators
     ssax
     sxml-transforms
     sxpath
     uri-common
     utils)

(define base-url "http://videos.arte.tv")
(define video-index "/de/videos/")

(define video-sre '(: "/de/videos/" (+ (~ #\/)) ".html" eos))
(define video-index-page-sre '(: "/de/videos?page=" (+ digit)))

(define *m* (make-parameter '()))

(define video-list-parser
  (make-html-parser 'start:
                    (lambda (tag attrs seed virtual?)
                      (let ((link (alist-ref 'href attrs)))
                        (if (and (equal? tag 'a)
                                 (not virtual?)
                                 link
                                 (irregex-match video-sre (car link)))
                            (cons `((url . ,(car link))) seed)
                            seed)))))

(define video-index-list-parser
  (make-html-parser 'start:
                    (lambda (tag attrs seed virtual?)
                      (let ((link (alist-ref 'href attrs)))
                        (if (and (equal? tag 'a)
                                 (not virtual?)
                                 link
                                 (irregex-match video-index-page-sre (car link)))
                            (cons `((url . ,(car link))) seed)
                            seed)))))

(define video-ref-url-parser
  (make-html-parser
   'start:
   (lambda (tag attrs seed v?)
     (let ((p-start #f))
       (cond
        ((equal? tag 'object) (cons (list 'url) seed))
        ((and (equal? tag 'param)
              (pair? seed)
              (alist-ref 'url seed)
              (equal? (alist-ref 'name attrs) '("movie"))
              (alist-ref 'value attrs)) =>
              (lambda (m)
                (let* ((uri (uri-reference (car m)))
                       (ref (alist-ref 'videorefFileUrl (uri-query uri)))
                       (player-url (uri->string (update-uri uri query: '()))))
                  (alist-update 'player player-url
                                (alist-update 'url ref seed)))))
        ((and (equal? tag 'p)
              (not (alist-ref 'info seed)))
         (cons (list 'info) seed))
        (else seed))))
   'text:
   (lambda (text seed)
     (cond ((alist-ref 'info seed)
            => (lambda (i)
                 (if (null? i)
                     (alist-update 'info text seed)
                     seed)))
           (else seed)))))

(define (stream-url-grabber)
  (let ((document (ssax:xml->sxml (current-input-port) '())))
    (foldts (lambda (seed node)
              (cond ((and (equal? 'video (car node))
                          (equal? (alist-ref 'lang (cdadr node)) '("de"))
                          (alist-ref 'ref (cdadr node))) =>
                          (lambda (r)
                            (cons `((url . ,(car r))) seed)))
                    (else seed)))
            (lambda (parent-seed last-kid-seed node) last-kid-seed)
            (lambda (seed atom) seed)
            '()
            document)))

(define (stream-metadata-grabber)
  (let* ((document (ssax:xml->sxml (current-input-port) '()))
         (video-raw-url
          ((sxpath '(video (*or* name url dateExpiration dateVideo numberOfViews urls))) document '()))
         (url-qualities (map (lambda (u) (list (string->symbol (cadr (cadadr u)))
                                               (caddr u)))
                             (alist-ref 'urls video-raw-url)))
         (dateExpiration (alist-ref 'dateExpiration video-raw-url))
         (dateExpiration (and dateExpiration
                              (time->seconds (date->time (scan-date (car dateExpiration) "~a, ~d ~b ~Y ~H:~M:~S ~z")))))
         (dateVideo (alist-ref 'dateVideo video-raw-url))
         (dateVideo (and dateVideo
                         (time->seconds (date->time (scan-date (car dateVideo) "~a, ~d ~b ~Y ~H:~M:~S ~z"))))))
    (alist-update! 'name (car (alist-ref 'name video-raw-url)) video-raw-url)
    (alist-update! 'url (car (alist-ref 'url video-raw-url)) video-raw-url)
    (alist-update! 'numberOfViews (string->number (car (alist-ref 'numberOfViews video-raw-url))) video-raw-url)
    (alist-update! 'dateExpiration dateExpiration video-raw-url)
    (alist-update! 'dateVideo dateVideo video-raw-url)
    (alist-update 'urls url-qualities video-raw-url)))

;; (define link-list (delete-duplicates (with-input-from-file "videos.html" (lambda () (video-list-parser '())))))
;; (define video-ref (with-input-from-file "grace.html" (lambda () (video-ref-url-parser '()))))
;; (define stream-ref (with-input-from-file "ref.xml" stream-url-grabber))
;; (define video-metadata (with-input-from-file "asPlayer.xml" stream-metadata-grabber))

(define (alist-merge a b)
  (fold
   (lambda (e s)
     (alist-update (car e) (cdr e) s equal?)) b a))

(define (grab-url url #!key (retries 5)
                            (delay (random 4))
                            (reader (lambda () (read-lines)))
                            (writer #f)
                            (seed '()))
  (printf "Getting ~a" (uri->string url))
  (condition-case
   (parameterize ((max-retry-attempts retries)
                  (client-software (list
                                    (string-split "Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405" " ();"))))
                 (let ((urls (with-input-from-request url writer reader)))
                   (newline)
                   (cond ((and (pair? urls) (every list? urls))
                          (fold (lambda (url s)
                                  (cons (alist-merge url seed) s))
                                '()
                                urls))
                         (else (alist-merge urls seed)))))
   (e (exn http server-error) (printf " Server error ~a~%" (response-reason ((condition-property-accessor 'server-error 'response) e))) '())
   (e (user-interrupt) (signal e))
   (e () (signal e))))

(define get-all
  (lambda (urls reader hof)
    (hof (lambda (u)
           (let ((url (uri-reference (alist-ref 'url u))))
             (grab-url
              (if (relative-ref? url)
                  (uri-relative-to url (uri-reference base-url))
                  url)
              seed: u
              reader: reader)))
         urls)))

(define (all-films start-url)
  (let loop ((urls `(((url . ,start-url))))
             (readers `(,(lambda () (video-index-list-parser '()))
                        ,(lambda () (video-list-parser '()))
                        (,(lambda () (video-ref-url-parser '())) ,map)
                        ,stream-url-grabber
                        (,stream-metadata-grabber ,map)))
             (seen-urls '()))
    (if (null? readers) urls
        (let* ((reader (car readers))
               (hof? (pair? reader))
               (hof (if hof? (cadr reader) append-map))
               (reader (if hof? (car reader) reader))
               (new-urls (delete-duplicates (filter (lambda (u) (not (member u seen-urls))) urls))))
          (loop (get-all new-urls reader hof) (cdr readers) (alist-merge new-urls seen-urls))))))


(define (expired? movie)
  (let ((exp-date (alist-ref 'dateExpiration movie)))
    (and exp-date (< exp-date (current-seconds)))))

(define (reload-movies! #!optional filename)
  (let ((movies (if (and filename (file-exists? filename))
                    (map cdr (with-input-from-file filename read))
                    (all-films video-index))))
    (*m* (map cons (iota (length movies)) (delete-duplicates
                                           (append (map cdr (*m*))
                                                   (sort (filter (lambda (m) (not (expired? m))) movies)
                                                         (lambda (a b)
                                                           (< (alist-ref 'dateVideo a)
                                                              (alist-ref 'dateVideo b))))))))
    (length (*m*))))

(define (movie-title movie)
  (and-let* ((url (alist-ref 'url movie))
             (raw-title (irregex-match `(: ,base-url ,video-index ($ (+ any)) ".html") url))
             (title (if (irregex-match-valid-index? raw-title 1) (irregex-match-substring raw-title 1) (alist-ref 'name movie))))
    title))

(define (movie-stream-url movie)
  (let* ((urls (alist-ref 'urls movie))
         (sd (alist-ref 'sd urls))
         (hd (alist-ref 'hd urls)))
    (car (or hd sd))))

(define (rtmpdump movie #!optional filename)
  (let* ((title (movie-title movie))
         (p (make-progress-bar frame: (string-append title ": " "[~a~a~a~a|~a]"))))
    (with-input-from-pipe (sprintf "rtmpdump -r ~s -o ~a 2>&1"
                                   (movie-stream-url movie)
                                   (or filename (sprintf "~a.flv" (movie-title movie))))
                          (lambda ()
                            (let loop ((l (read-line)))
                              (unless (eof-object? l)
                                      (let* ((progress (irregex-match '(: (+ any) "(" ($ (+ any)) "%)") l))
                                             (percent (and progress
                                                           (irregex-match-valid-index? progress 1)
                                                          (floor (string->number (irregex-match-substring progress 1))))))
                                        (when percent
                                              (set! (progress-bar-value p) percent)
                                              (show-progress-bar p))
                                        (loop (read-line)))))
                            (finish-progress-bar! p)))))

(define (save-movies #!optional
                     (filename (sprintf "arte+7-~a.sexpr"
                                        (format-date #f "~1" (seconds->date (current-seconds))))))
  (with-output-to-file filename (lambda () (pp (*m*)))))

(define (extract-from-alist alist syms #!optional (default identity) (equal equal?))
  (map (lambda (s) (or (alist-ref s alist equal) (default s))) syms))

(define (search #!optional (item 'any) (next print-entries) (movies (*m*)))
  (next (filter
         (lambda (i)
           (irregex-search (irregex item 'i) (alist-ref 'name (cdr i))))
         movies)))

(define (seconds->date-string #!optional (secs (current-seconds)))
  (format-date #f "~1" (seconds->date (exact->inexact secs)))) ; XXX This is stupid seconds->date seems to expect a flonum

(define (format-list-entry port e)
  (fprintf port "~a\t~a\t~a\t~a~%"
           (car e)
           (alist-ref 'name (cdr e))
           (seconds->date-string (alist-ref 'dateVideo (cdr e)))
           (seconds->date-string (alist-ref 'dateExpiration (cdr e)))))

(define (print-entries e)
    (printf  "#\tTitel\t\t\t\tErschienen\tWird gelöscht am~%")
  (for-each (lambda (e) (format-list-entry (current-output-port) e)) e))

(define (download m #!optional filename)
  (for-each (lambda (m) (rtmpdump (cdr m) filename)) m))

(define (info res)
  (for-each (lambda (m)
              (print (alist-ref 'name (cdr m)))
              (print "---")
              (print (alist-ref 'info (cdr m)))
              (newline))
            res))



(define (delete-last-word line pos)
  (let* ((del-pos (or (string-index-right
                       line
                       (char-set-union char-set:whitespace
                                       char-set:punctuation)
                       pos)
                      0))
         (left-part (if (> del-pos 0) (string-take line del-pos)
                        ""))
         (npos (- pos (- pos
                         del-pos))))
    (values (string-append left-part (string-drop line pos))
            npos)))

(define (cw prompt in out line pos exit offset)
  (receive (l p) (delete-last-word line pos)
           (list prompt in out l p exit offset)))


(let ((old (current-input-port)))
  (current-input-port (make-parley-port old)))

(add-key-binding! #\x17 cw)

(completion-choices
 (lambda (input position last-word)
   (append '("reload-movies!" "search" "info" "download" "print-entries" "save-movies") '()))) ;; XXX Add movie titles here

(word-class '(: (? (* any) whitespace) ($ (+ (~ "()[]")))))

(add-key-binding! #\tab auto-completion-handler)
(repl-prompt (lambda () "#;arte-scraper> "))
(repl)
